import Stats from 'https://cdn.bootcdn.net/ajax/libs/stats.js/r17/Stats.min.js'
import { WEB3D } from '../src/bundle.js'
import { modelData } from '../src/modelData.js'
import { buildingStyles } from '../src/style.building.js'

const web3D = new WEB3D('#web3D', { op: 123 }).addHelpers()
const run = {
  /**
   * 显示运行状态监视器
   * @param position 显示的面板的位置，默认为[9, 9]
   * @param panel 显示的面板（1~3），默认为 2
   */
  stats(position = [9, 9], panel = 2) {
    const stats = new Stats()
    stats.showPanel(panel)
    stats.dom.style.top = position[0] + 'px'
    stats.dom.style.left = position[1] + 'px'
    document.body.appendChild(stats.dom)
    requestAnimationFrame(animate)

    function animate() {
      stats.begin()
      stats.end()
      requestAnimationFrame(animate)
    }
  },
  /**
   * 测试生成建筑
   * @param basicPlan 生成的平面坐标点数据 [number,number][ ][ ]
   * @param options - `name` 样式名称， `minHeight` 生成的最小高度， `maxHeight` 生成的最大高度， `x` 生成的X轴间距， `y` 生成的Y轴间距， `count` 每个样式生成的数量
   */
  test(basicPlan, options) {
    const plans = []
    const name = options.name
    const min = options.minHeight || 0
    const max = options.maxHeight || h
    const x = options.x || 100
    const y = options.y || 50
    const c = options.count || 1
    if (name) {
      for (let i = 0; i < c; i++) {
        pushNamesTo(plans, basicPlan, name, min, max, c, x, y)
      }
    } else {
      let n = 0
      for (const name in buildingStyles) {
        pushNamesTo(plans, basicPlan, name, min, max, c, x * n++, y)
      }
    }
    web3D.update(plans)

    function pushNamesTo(a, plan, name, minHeight, maxHeight, count, positionX, spacingY) {
      const u = count > 1 ? (maxHeight - minHeight) / (count - 1) : 0
      for (let i = 0; i < count; i++) {
        a.push({
          name: `[p=${name} h=${minHeight + i * u}]`,
          loops: plan.map((loop) => loop.map((p) => [p[0] + positionX, p[1] + spacingY * i])),
        })
      }
    }
  },
  /**
   * 从解析的城市数据 modelData 中读取指定数量的建筑数据，随机选择样式并测试生成
   * @param n 读取的建筑数量
   */
  cityTest(n = 0) {
    const plans = modelData.map((a) => {
      return {
        loops: [a[0]],
        name: `building [h=${a[1].toFixed(2)}}]`,
      }
    })

    const l = plans.length
    if (n > l) n = l
    if (n > 0) {
      plans.sort(() => Math.random() - 0.5)
      plans.length = n
      const size = JSON.stringify(plans).length
      const text =
        size > 1048576
          ? (size / 1048576).toFixed(2) + ' MB'
          : size > 1024
          ? (size / 1024).toFixed(2) + ' KB'
          : `${size} B`
      console.log(`cityTest: loading ${n} / ${l} buildings from modelData (${text}) ...`)
    }
    web3D.update(plans)
  },
}

////////////////////// TEST ZONE //////////////////////

run.stats()

const basicPlan = [
  [
    [-20, -20],
    [0, -20],
    [20, -10],
    [40, -10],
    [40, 10],
    [10, 10],
    [0, 5],
    [-20, 5],
  ],
  // [
  //   [0, -10],
  //   [-10, -10],
  //   [-10, 0],
  //   [0, 0],
  // ],
]

run.test(basicPlan, { maxHeight: 80, count: 6 })

// run.cityTest(1000)
