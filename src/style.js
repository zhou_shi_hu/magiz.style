import { buildingStyles } from "./style.building.js";
export { parseStyle, mutate, };
const features = {
    uses: {
        house: ['住宅'],
        commerce: ['商场'],
        office: ['办公'],
        hotel: ['酒店'],
        hospital: ['医院'],
        school: ['学校'],
        industry: ['工厂'],
        warehouse: ['仓库'],
        monument: ['纪念碑'],
    },
    tags: {
        simple: ['简单'],
        complex: ['复杂'],
        highRise: ['高层'],
        mediumRise: ['多层'],
        lowRise: ['低层'],
        vertical: ['垂直'],
        horizontal: ['水平'],
        random: ['随机'],
    }
};
function parseStyle(name, buildingHeight, floorHeight, options = {}) {
    const style = getStyle(name);
    const result = { blocks: [], parts: [] };
    const params = Object.assign({ BH: buildingHeight, FH: floorHeight }, style.units);
    const heights = style.sections.map((s) => {
        if (s.height) {
            const h = parse(s.height);
            buildingHeight -= h;
            return h;
        }
    });
    let elevation = 0;
    for (let n = style.sections.length - 1; n >= 0; n--) {
        const s = style.sections[n];
        const sh = params.SH = heights[n] || buildingHeight;
        if (s.parts)
            parsePartsTo(result.parts, s.parts, elevation);
        s.floors.forEach(f => {
            let fh, fn;
            if (options.fixedFloorHeight) {
                fh = params.FH = f.height ? parse(f.height) : floorHeight;
                fn = Math.floor(sh / fh);
            }
            else {
                fh = f.height ? parse(f.height) : floorHeight;
                fn = Math.floor(sh / fh);
                fh = params.FH = sh / fn;
            }
            const mod = f.mod;
            let range = [0, fn];
            if (mod) {
                if (mod.floorCount) {
                    range = [0, parse(mod.floorCount)];
                }
                else if (mod.floorEnds) {
                    range = [parse(mod.floorEnds[0]), fn + parse(mod.floorEnds[1])];
                }
            }
            const floorBlocks = [];
            const floorParts = [];
            if (f.styles) {
                f.styles.forEach(s => {
                    if (s.blocks)
                        parseBlocksTo(floorBlocks, s.blocks, 0);
                    if (s.parts)
                        parsePartsTo(floorParts, s.parts, 0);
                });
            }
            else {
                if (f.blocks)
                    parseBlocksTo(floorBlocks, f.blocks, 0);
                if (f.parts)
                    parsePartsTo(floorParts, f.parts, 0);
            }
            for (let i = range[0]; i < range[1]; i++) {
                const z = elevation + i * fh;
                let currentBlocks = setZ(floorBlocks, z);
                let currentParts = setZ(floorParts, z);
                if (mod) {
                    check(i, (style) => {
                        if (style.blocks)
                            parseBlocksTo(currentBlocks = [], style.blocks, z);
                        if (style.parts)
                            parsePartsTo(currentParts = [], style.parts, z);
                    }, mod.style);
                    check(i, (v) => {
                        const x = parse1OR2(v);
                        floorBlocks.forEach(b => b.scaleXY = x);
                        floorParts.forEach(p => p.scaleXY = x);
                    }, mod.scaleXY);
                    check(i, (v) => {
                        const x = parse(v);
                        floorBlocks.forEach(b => b.rotate = x);
                        floorParts.forEach(p => p.rotate = x);
                    }, mod.rotate);
                    check(i, (v) => {
                        const x = parse1OR2(v);
                        floorBlocks.forEach(b => b.moveXY = x);
                        floorParts.forEach(p => p.moveXY = x);
                    }, mod.moveXY);
                    check(i, (v) => {
                        const x = parse(v);
                        floorBlocks.forEach(b => b.moveZ = x);
                        floorParts.forEach(p => p.moveZ = x);
                    }, mod.moveZ);
                }
                result.blocks.push(...currentBlocks);
                result.parts.push(...currentParts);
            }
        });
        elevation += sh;
    }
    return result;
    function setZ(a, z) {
        let k;
        return a.map(x => {
            const result = {};
            for (k in x)
                result[k] = x[k];
            result.moveZ += z;
            return result;
        });
    }
    function parseBlocksTo(result, blocks, z) {
        blocks.forEach(x => {
            result.push({
                depth: parse(x.depth),
                as: x.as || 'basic',
                moveZ: parse(x.moveZ) + z,
                moveXY: parse1OR2(x.moveXY),
                scaleXY: parse1OR2(x.scaleXY),
                rotate: parse(x.rotate)
            });
        });
    }
    function parsePartsTo(a, parts, z) {
        parts.forEach(x => {
            a.push({
                elements: parseElements(x.elements),
                split: parseSplit(x.split),
                moveZ: parse(x.moveZ) + z,
                moveXY: parse1OR2(x.moveXY),
                scaleXY: parse1OR2(x.scaleXY),
                rotate: parse(x.rotate),
                along: x.along
            });
        });
        function parseSplit(s) {
            const result = {
                padding: parse1OR2(s.padding),
                minLength: parse(s.minLength),
                maxLength: parse(s.maxLength)
            };
            if (s.number) {
                result.number = parse(s.number);
            }
            else if (s.spacing) {
                result.spacing = s.spacing.map(parse);
            }
            return result;
        }
        function parseElements(a) {
            return a.map(x => {
                const result = {
                    copy: x.copy ? x.copy.map(p3D => p3D.map(parse)) : [],
                    moveZ: parse(x.moveZ),
                    moveXY: parse1OR2(x.moveXY),
                    scaleXY: parse1OR2(x.scaleXY),
                    rotate: parse(x.rotate)
                };
                if (x.lines) {
                    result.lines = [x.lines[0].map(p3D => p3D.map(parse)), x.lines[1] || 'basic'];
                }
                else if (x.box) {
                    result.box = [x.box[0].map(parse), x.box[1] || 'basic'];
                }
                else if (x.loops) {
                    result.loops = [parse1OR2(x.loops[0]), x.loops[1] || 'basic'];
                }
                return result;
            });
        }
    }
    function check(n, correct, modParsed) {
        if (modParsed) {
            modParsed.forEach((a) => {
                let u, s;
                if (Array.isArray(a[0])) {
                    u = a[0][0];
                    s = a[0][1];
                }
                else {
                    u = a[0];
                    s = 0;
                }
                if (n >= s && (n - s) % u == 0)
                    correct(a[1]);
            });
        }
    }
    function parse1OR2(x) {
        return !x ? [0, 0] : (Array.isArray(x) ? x.map(parse) : new Array(2).fill(parse(x)));
    }
    function parse(ns) {
        var n = 0;
        if (!ns) { }
        else if (typeof ns == 'string') {
            let s = ns.replace(/([\d\.]*)~([\d\.]+)/g, (m, a, b) => [a || 0, b].map(Number).reduce((x, y) => x + (y - x) * Math.random()).toString());
            const regexp = new RegExp(Object.keys(params).map(k => `([\\d\\.]*${k})`).join('|'), 'g');
            s = s.replace(regexp, ((m) => {
                const n = /[\d\.]+/.exec(m);
                return (n ? Number(n[0]) * params[m.slice(n[0].length)] : params[m]).toString();
            }));
            s = s.replace(/[a-z]/ig, '');
            s = parseBrackets(s).replace(/[\(\)]/ig, '');
            n = Number(parseFomula(s));
            if (Number.isNaN(n)) {
                console.log(`[FOMULA ERROR] ${ns}`);
                n = 0;
            }
        }
        else
            n = ns;
        return n;
        function parseBrackets(s) {
            return s.replace(/\([^\)]+\)/g, (m) => parseFomula(parseBrackets(m.slice(1, -1))));
        }
        function parseFomula(s) {
            return s.replace(/([\d\.]+ *\* *[\d\.]+)|([\d\.]+ *\/ *[\d\.]+)|([-\d\.]+ *\+ *[\d\.]+)|([-\d\.]+ *- *[\d\.]+)|([\d\.]* *\+- *[\d\.]+)/g, (m, m1, m2, m3, m4, m5) => {
                let v = 0;
                if (m1) {
                    const a = m.split('*').map(Number);
                    v = a[0] * a[1];
                }
                else if (m2) {
                    const a = m.split('/').map(Number);
                    v = a[0] / a[1];
                }
                else if (m3) {
                    const a = m.split('+').map(Number);
                    v = a[0] + a[1];
                }
                else if (m4) {
                    const a = m.split('-').map(Number);
                    v = a[0] - a[1];
                }
                else if (m5) {
                    const a = m.split('+-').map(Number);
                    v = Math.random() > 0.5 ? a[0] + a[1] : a[0] - a[1];
                }
                return v.toString();
            });
        }
    }
}
function getStyle(name) {
    let s = buildingStyles[name];
    if (!s) {
        const a = Object.keys(buildingStyles);
        const n = a[Math.floor(a.length * Math.random())];
        s = buildingStyles[n];
        console.warn(`Can not find '${name}', fall back to random style.`);
    }
    return s;
}
function mutate(from, genes, to) {
    let mutateSuccess = false;
    const result = deepClone(getStyle(from));
    genes.forEach(g => {
        if (result.sections[g.section]) {
            if (g.all) {
                const x = g.all;
                const m = Array.isArray(x) ? getStyle(x[0]).sections[x[1]] : x;
                if (m) {
                    result.sections[g.section] = m;
                    mutateSuccess = true;
                }
            }
            if (g.parts) {
                const x = g.parts;
                const m = Array.isArray(x) ? getStyle(x[0]).sections[x[1]].parts : x;
                if (m) {
                    result.sections[g.section].parts = m;
                    mutateSuccess = true;
                }
            }
            if (g.floors) {
                const x = g.floors;
                const m = Array.isArray(x) ? getStyle(x[0]).sections[x[1]].floors : x;
                if (m) {
                    result.sections[g.section].floors = m;
                    mutateSuccess = true;
                }
            }
        }
    });
    if (mutateSuccess) {
        buildingStyles[to] = result;
        console.log(`[MUTATE] New style added as '${to}'`);
    }
    function deepClone(x) {
        if (typeof x === 'object' && x !== null) {
            if (Array.isArray(x)) {
                return x.map(xx => deepClone(xx));
            }
            else {
                const clone = {};
                for (const k in x)
                    clone[k] = deepClone(x[k]);
                return clone;
            }
        }
        else
            return x;
    }
}
//# sourceMappingURL=style.js.map