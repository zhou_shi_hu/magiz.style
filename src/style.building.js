export { buildingStyles };
const buildingStyles = {
    default: {
        units: { SP: 3.6 },
        sections: [
            {
                height: '3',
                floors: [{ blocks: [{ depth: 0.2 }, { depth: '1.5', as: 'basic_wall', moveZ: 0.2 }] }],
            },
            {
                floors: [
                    {
                        blocks: [
                            { depth: 0.2, scaleXY: -1, moveZ: '1FH - 0.2' },
                            { depth: '1FH -0.2', as: 'curtain_wall' },
                            { depth: -0.5, scaleXY: -0.2 },
                        ],
                    },
                    {
                        styles: [{ blocks: [{ depth: '1SH + 1.2', scaleXY: [-1.5, 6], moveZ: -0.8 },] }],
                        mod: { floorCount: 1 }
                    },
                ],
            },
            {
                height: '0.2BH',
                floors: [
                    { id: '核心筒', blocks: [{ depth: '1BH', scaleXY: 5 }], mod: { floorCount: 1 } },
                    { blocks: [{ depth: 0.1, scaleXY: -1 }] },
                ],
                parts: [{
                        elements: [{
                                box: [[1, 1, '1SH']],
                            }],
                        split: { spacing: [8] }
                    }]
            },
        ],
    },
    normal: {
        sections: [
            {
                floors: [
                    {
                        id: '顶部玻璃围栏',
                        blocks: [
                            { depth: 0.4, scaleXY: -0.5 },
                            { depth: '1.2', as: 'curtain_wall', moveZ: 0.4 },
                        ],
                    },
                ],
                height: '3',
            },
            {
                floors: [
                    {
                        id: '整段玻璃幕墙',
                        blocks: [{ depth: '1SH', scaleXY: [1.5, 0], as: 'glass_wall' }], mod: { floorCount: 1 }
                    },
                    {
                        id: '楼板层',
                        blocks: [{ depth: -0.5, scaleXY: 0.5 }],
                        parts: [{
                                id: '扶手',
                                elements: [
                                    { loops: [0, 'red'], moveZ: 1.2, },
                                    { lines: [[[0, 0.2, 0], [0, 0.2, 1.1], [0, 0, 1.2],]] },
                                ],
                                split: { spacing: [3] },
                                scaleXY: 0.5,
                            }],
                        mod: {
                            style: [
                                [3, {
                                        blocks: [{ depth: 0.1, scaleXY: -1 }],
                                        parts: [{
                                                id: '扶手',
                                                elements: [
                                                    { loops: [0.5], moveZ: 1.2 },
                                                    { lines: [[[0, 0.2, 0], [0, 0.2, 1.1], [0, 0, 1.2]]], scaleXY: 0.5 },
                                                ],
                                                split: { spacing: [3] },
                                                scaleXY: 0.5,
                                            }],
                                    }],
                            ],
                        },
                    },
                ],
                parts: [
                    {
                        id: '整段东西向立柱',
                        elements: [{ box: [[0.4, 0.6, '1SH'], 'light'] }], split: { spacing: [1.5] }, along: 'we'
                    },
                    {
                        id: '整段南北向立柱',
                        elements: [{ box: [[1, 1, '1SH'], 'light'] }],
                        split: { spacing: [3], padding: 0.8 },
                        along: 'ns',
                    },
                ],
            },
            {
                height: '0.2BH',
                floors: [
                    { id: '核心筒', blocks: [{ depth: '1BH', scaleXY: 5 }], mod: { floorCount: 1 } },
                    { blocks: [{ depth: 0.1, scaleXY: -1 }] },
                ],
            },
        ],
    },
};
//# sourceMappingURL=style.building.js.map