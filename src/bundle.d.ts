/** @copyright (c) 2023 周曦 @license GPL v3 */
import * as THREE from 'three';
import * as CONTROL from 'three/examples/jsm/controls/OrbitControls.js';
/** @copyright (c) 2023 周曦 @license GPL v3 */
/** 该部分为纯数学结构模型，单位为米，忽略与显示相关的功能。从平面生成空间结构的逻辑 = 2D平面组合逻辑 + 基于层和阵列的3D组合逻辑 */
import Flatten from "@flatten-js/core";
interface style {
    type: string;
    geometry: number[];
    trunkRadius: number;
    rotate?: [
        number,
        number,
        number
    ];
    add?: {
        scale?: [
            number,
            number,
            number
        ];
        rotate?: [
            number,
            number,
            number
        ];
        translate?: [
            number,
            number,
            number
        ];
    }[];
}
// TREE 为 geometry ，方便后续进行合并以提升大量对象渲染的效率
declare class TREE {
    trunks: THREE.ConeGeometry[];
    leafs: THREE.BufferGeometry[];
    constructor(input: style);
    rotate(g: THREE.BufferGeometry, r?: [
        number,
        number,
        number
    ]): void;
}
declare class FOREST {
    trees: TREE[];
    positions: {
        pattern: number;
        point: [
            number,
            number,
            number
        ];
        height: number;
        rotate: number;
    }[];
    constructor(points: [
        number,
        number
    ][][], options?: {
        styles?: string[];
        height?: [
            number,
            number
        ];
        evaluation?: number;
    });
}
type ns = number | string;
type blockColors = keyof (typeof buildingColors)["blocks"];
type lineColors = keyof (typeof buildingColors)["lines"];
/** @see {@link basicParsed} */
interface basic {
    /** 沿XY轴的缩放距离，单值表示两个方向缩放距离相同 */
    scaleXY?: [
        x: ns,
        y: ns
    ] | ns;
    /** 围绕Z轴的旋转角度 */
    rotate?: ns;
    /** 沿XY轴的移动距离 */
    moveXY?: [
        x: ns,
        y: ns
    ];
    /** 沿Z轴的移动距离，很常用所以单独输入 */
    moveZ?: ns;
}
/** @see {@link elementParsed} */
interface element extends basic {
    /** 按轴向偏缩放界生成的线元素 */
    loops?: [
        scaleXY: [
            x: ns,
            y: ns
        ] | ns,
        color?: lineColors
    ];
    /** 按坐标点生成线元素 */
    lines?: [
        points: [
            x: ns,
            y: ns,
            z: ns
        ][],
        color?: lineColors
    ];
    /** 按长宽高生成块元素 */
    box?: [
        scale: [
            w: ns,
            h: ns,
            d: ns
        ],
        color?: blockColors
    ];
    /** 复制自身到坐标点位置 */
    copy?: [
        x: ns,
        y: ns,
        z: ns
    ][];
}
/** `elements` `split` `scaleXY?` `rotate?` `moveXY?` `moveZ?` @see {@link partParsed} */
interface partStyle extends basic {
    /** 仅用于标识，方便编辑 */
    id?: string;
    /** `loops? | lines? | box?` `copy?` `color?` `scaleXY` `rotate?` `moveXY?` `moveZ?` */
    elements: element[];
    /** 按边的方向生成（东西或南北） */
    along?: "we" | "ns";
    /** 按边界阵列的参数 */
    split: {
        /** 按段数等分 */
        number?: ns;
        /** 按长度组合进行划分 */
        spacing?: ns[];
        /** 每条线段的两端缩进 */
        padding?: [
            start: ns,
            end: ns
        ] | ns;
        /** 忽略的最小线段长度 */
        minLength?: ns;
        /** 忽略的最大线段长度 */
        maxLength?: ns;
    };
}
/** @see {@link basic} */
interface basicParsed {
    scaleXY: [
        x: number,
        y: number
    ];
    rotate: number;
    moveXY: [
        x: number,
        y: number
    ];
    moveZ: number;
}
/** @see {@link partStyle} */
interface partParsed extends basicParsed {
    elements: elementParsed[];
    split: {
        number?: number;
        spacing?: number[];
        padding: [
            start: number,
            end: number
        ] | 0;
        minLength: number;
        maxLength: number;
    };
    along: partStyle["along"];
}
/** @see {@link element} */
interface elementParsed extends basicParsed {
    loops?: [
        scaleXY: [
            x: number,
            y: number
        ],
        as: lineColors
    ];
    lines?: [
        points: [
            x: number,
            y: number,
            z: number
        ][],
        as: lineColors
    ];
    box?: [
        scale: [
            w: number,
            h: number,
            d: number
        ],
        as: blockColors
    ];
    copy: [
        x: number,
        y: number,
        z: number
    ][];
}
/** @see {@link blockStyle} */
interface blockParsed extends basicParsed {
    depth: number;
    as: blockColors;
}
// 解析结果按材质分类缓存，便于后续 merge
// blockParsed 和 elementParsed 必须要 moveZ 属性
// 基于建筑基底生成唯一的 Geometry，基于不同材质和每类的数量创建 instanceMesh
interface resultParsed {
    blocks: blockParsed[];
    parts: partParsed[];
}
////// 正确的布尔运算，face 的方向非常关键，因此创建时就修正顺序 //////
////// 由于 THREE.Shape 仅支持 Vector2，全部计算也基于2D //////
type originalPointsRotated = [
    [
        number,
        number
    ],
    number
];
declare class ISLAND {
    name: string;
    polygon: Flatten.Polygon;
    loops: [
        number,
        number
    ][][];
    size: [
        number,
        number
    ];
    color: string;
    id: string;
    constructor(input: {
        name: string;
        loops?: number[][][];
        polygon?: Flatten.Polygon;
        color?: string;
        id?: string;
    });
    /** 返回将中点重置到原点后的边界点阵列 */
    getOriginalPointsRotated(split: partParsed["split"], along: partParsed["along"]): originalPointsRotated[];
    /** 用于在移动到原点缩放变形，返回将中点重置到原点后的坐标数据和还原矢量 */
    resetCenter(inner?: boolean): {
        center2D: [
            number,
            number
        ];
        points2D: [
            number,
            number
        ][][];
    };
}
interface params {
    h: number; //  buildingHeight
    f: number; //  floorHeight
    e: number; //  elevation
    p: string; //  pattern
    s: string[]; //  substract
    i: string[]; //  intersect
    u: boolean; //  unify
    v: boolean; //  void
}
interface structureData {
    params: params;
    color: string;
    shapes: ISLAND[];
    merged: ISLAND[];
    mergedPolygons: Flatten.Polygon[]; // merged须分批遍历，每次结果都须缓存到对象
    style: resultParsed;
}
declare class STRUCTURE {
    land: {
        [name: string]: structureData;
    };
    /** 按名称分类，相同名称的不同平面具有相同的高度和样式 */
    building: {
        [name: string]: structureData;
    };
    forest?: FOREST;
    // shapes 为每个面的数据，可能有不同的参数设置，STRUCTURE 先对这些面进行分类，同名的才进行整合
    // shapes = [{id, name, loops} ...]
    constructor(islands: ISLAND[], generateStyles?: boolean);
    mergeShapes(): void;
    // 生成树林原始数据
    generateForestData(spacing?: number): void;
}
/** 考虑运行效率最高，样式中仅使用预定义的材质 */
declare const buildingColors: {
    blocks: {
        white: THREE.MeshLambertMaterial;
        light: THREE.MeshLambertMaterial;
        basic: THREE.MeshLambertMaterial;
        dark: THREE.MeshLambertMaterial;
        black: THREE.MeshLambertMaterial;
        glass: THREE.MeshLambertMaterial;
        logo: THREE.MeshLambertMaterial;
        basic_wall: THREE.MeshLambertMaterial;
        glass_wall: THREE.MeshLambertMaterial;
        curtain_wall: THREE.MeshLambertMaterial;
    };
    lines: {
        basic: THREE.MeshLambertMaterial;
        red: THREE.MeshLambertMaterial;
    };
};
interface updateData {
    name: string;
    loops: [number, number][][];
    color?: string;
    id?: string;
}
declare const webOptions: {
    tips: boolean;
    logRenderInfo: boolean;
    logFileSize: boolean;
    autoRotate: boolean;
    autoShadow: boolean;
    cameraPosition: number[];
    cameraLookAt: number[];
    sunDistance: number;
    sunPhiRange: number[];
    backgroundColor: string;
    seasonColors: {
        [type: string]: [string, string, string, string];
    };
    lightColors: {
        [time: string]: [string, number, number];
    };
};
declare class WEB3D {
    renderer: THREE.WebGLRenderer;
    camera: THREE.PerspectiveCamera;
    controls: CONTROL.OrbitControls;
    cores: CORE[];
    playing: CORE;
    sunPosition: THREE.Vector3;
    options: typeof webOptions;
    /**
     * 封装各个场景到类型实例，作为接口
     * @param cssSelector 通过querySelector绑定Three.js
     * @param options 设置参数
     */
    /**
     * 封装各个场景到类型实例，作为接口
     * @param cssSelector 通过querySelector绑定Three.js
     * @param options 设置参数
     */
    constructor(cssSelector: string, options?: any);
    update(shapes: updateData[], coreID?: number): WEB3D;
    addHelpers(coreID?: number): WEB3D;
    setCamera(position?: [number, number, number], lookAt?: [number, number, number]): WEB3D;
    /** @param ratio - 按 0 到 1 划分四季 */
    /** @param ratio - 按 0 到 1 划分四季 */
    setSeason(ratio: number): WEB3D;
    /** @param ratio - 按 0 到 1 划分一天 */
    /** @param ratio - 按 0 到 1 划分一天 */
    setTime(ratio: number): THREE.Vector3;
    setCore(i: number): void;
    resizeScene(w?: number, h?: number): void;
}
declare const coreOptions: {
    highPerformance: boolean;
    addParts: boolean;
    showEdge: boolean;
};
/** 每个CORE为一个场景，并包含了所需的模型、光、机构数据等元素 */
declare class CORE {
    parent: WEB3D;
    scene: THREE.Scene;
    lights: {
        directional: THREE.DirectionalLight;
        ambient: THREE.AmbientLight;
    };
    animations: Function[];
    meshes: THREE.Group;
    ground: THREE.Mesh;
    structure?: STRUCTURE;
    options: typeof coreOptions;
    constructor(parent: WEB3D, animations?: Function[], options?: any);
    setGround(options: {
        width?: number;
        opacity?: number;
        color?: string;
    }): void;
    setShadowArea(width: number): void;
    addGroup(name: string): THREE.Group;
    addForestMeshes(wind?: {
        strength?: number;
        radian?: number;
    }): void;
    addLandMeshes(): void;
    addBuildingMeshes(): void;
}
export { WEB3D, buildingColors };
